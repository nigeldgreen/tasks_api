<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fillable = [
        'title',
        'notes',
        'client_id',
        'client_order',
        'user_id',
        'created_at'
    ];
    protected $casts = [
        'client_id' => 'integer',
        'user_id' => 'integer',
        'client_order' => 'integer',
    ];
    

    /**
     * Get the client that the project belongs to
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get all of the actions for the project.
     */
    public function actions()
    {
        return $this->hasMany(Action::class);
    }
}
