<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fillable = [
        'title',
        'notes',
        'created_at'
    ];
    protected $casts = [
        'order' => 'integer',
        'user_id' => 'integer',
    ];

    // Define relationships
    /**
     * Get all of the projects for the client.
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    /**
     * Get all of the actions for the client.
     */
    public function actions()
    {
        return $this->hasMany(Action::class);
    }
}
