<?php

namespace App\Http\Controllers;

use App\Action;
use App\Client;
use App\Project;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Show inbox and projects/actions from all clients
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request)
    {
        $inbox = [
            'actions' => Action::where('client_id', 0)->where('project_id', 0)->get(),
            'projects' => Project::where('client_id', 0)->with('actions')->get(),
        ];
        $clients = Client::with('projects.actions')->with('actions')->get();
        return response(['inbox' => $inbox, 'clients' => $clients], 200);
    }
}
