<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes;

    protected $dates = ['due',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fillable = [
        'title',
        'url',
        'due',
        'notes',
        'user_id',
        'client_id',
        'project_id',
        'project_order',
        'flagged',
        'created_at',
        'deleted_at'
    ];
    protected $casts = [
        'project_id' => 'integer',
        'client_id' => 'integer',
        'user_id' => 'integer',
        'project_order' => 'integer',
    ];

    /**
     * Get the project that the action belongs to
     *
     * @return belongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
    /**
     * Get the client that the action belongs to
     *
     * @return belongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
