<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    public function setup()
    {
        parent::setup();
        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
    }
    /** @test */
    public function can_get_all_clients_projects_and_actions()
    {
        Passport::actingAs(
            factory(User::class)->create(),
            ['get-clients']
        );
        $response = $this->json('GET', '/api/v1', [], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);

        $response->assertStatus(200);
        $json1 = [
            'inbox' => [
                'actions' => [
                    ['id' => 1]
                ]
            ]
        ];
        $response->assertJson($json1);
        $json2 = [
            'clients' => [
                [
                    'id' => 1,
                ],
                [
                    'id' => 2,
                    'projects' => [
                        [
                            'id' => 2,
                            'actions' => [
                                [
                                    'id' => 8
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $response->assertJson($json2);
    }
}
