<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ConnectionTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }
    /** @test */
    public function api_shows_correct_status()
    {
        $response = $this->get('/status');

        $response->assertStatus(200);
        $response->assertSee('Tasksapp API is IN');
    }

    /** @test */
    public function check_the_database_seeder()
    {
        $this->assertDatabaseHas('clients', [
            'id' => 1, 'title' => 'Test Client 1'
        ]);
        $this->assertDatabaseHas('projects', [
            'id' => 5, 'title' => 'Project 5'
        ]);
        $this->assertDatabaseHas('actions', [
            'id' => 6, 'title' => 'Action 6'
        ]);
    }
}
