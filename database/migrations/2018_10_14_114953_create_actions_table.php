<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Create the core columns for the Actions table
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('notes')->nullable();
            $table->date('due')->nullable();
            $table->string('url')->nullable();
            $table->boolean('flagged')->default(0);
            $table->integer('project_id')->nullable()->unsigned()->index();
            $table->integer('project_order')->default(0);
            $table->integer('client_id')->nullable()->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actions');
    }
}
