<?php

use App\Action;
use App\Client;
use App\Project;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Action::create([ 'title' => 'Action 1', 'client_id' => 0, 'project_id' => 0, 'user_id' => 1 ]);
        Action::create([ 'title' => 'Action 2', 'client_id' => 0, 'project_id' => 0, 'user_id' => 1 ]);
        $project1 = Project::create([ 'title' => 'Project 1', 'client_order' => 1, 'client_id' => 0, 'user_id' => 1 ]);
        $project1->actions()->createMany([
            [ 'title' => 'Action 3', 'project_order' => 1, 'user_id' => 1 ],
            [ 'title' => 'Action 4', 'project_order' => 2, 'user_id' => 1 ],
            [ 'title' => 'Action 5', 'project_order' => 3, 'user_id' => 1 ],
        ]);
        $client1 = Client::create([
            'id' => 1,
            'title' => 'Test Client 1',
            'notes' => 'A note',
            'order' => 1,
            'user_id' => 1
        ]);
        $client1->actions()->createMany([
            [ 'title' => 'Action 6', 'project_order' => 1, 'project_id' => 0, 'user_id' => 1 ],
            [ 'title' => 'Action 7', 'project_order' => 2, 'project_id' => 0, 'user_id' => 1 ],
        ]);
        $client2 = Client::create([
            'id' => 2,
            'title' => 'Test Client 2',
            'order' => 2,
            'user_id' => 1
        ]);
        $client2->projects()->createMany([
            [ 'title' => 'Project 2', 'client_order' => 1, 'user_id' => 1 ],
            [ 'title' => 'Project 3', 'client_order' => 2, 'user_id' => 1 ],
        ]);
        $client3 = Client::create([
            'id' => 3,
            'title' => 'Test Client 3',
            'order' => 3,
            'user_id' => 1
        ]);
        $client3->projects()->createMany([
            [ 'title' => 'Project 4', 'client_order' => 1, 'user_id' => 1 ],
            [ 'title' => 'Project 5', 'client_order' => 2, 'user_id' => 1 ],
        ]);
        $project2 = Project::find(2);
        $project2->actions()->createMany([
            [ 'title' => 'Action 8', 'project_order' => 1, 'user_id' => 1 ],
            [ 'title' => 'Action 9', 'project_order' => 2, 'user_id' => 1 ],
        ]);
        $project5 = Project::find(2);
        $project5->actions()->createMany([
            [ 'title' => 'Action 10', 'project_order' => 1, 'user_id' => 1 ],
            [ 'title' => 'Action 11', 'project_order' => 2, 'user_id' => 1 ],
            [ 'title' => 'Action 12', 'project_order' => 3, 'user_id' => 1 ],
        ]);
    }
}
